package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    TextField ipField;

    @FXML
    Button btnIp;

    private Boolean changeIpActive = false;

    public void btnPressed(ActionEvent event)
    {
        if (!changeIpActive)
        {
            ipField.setDisable(false);
            btnIp.setText("Apply");
        }else
        {
            Messager.setIp(ipField.getText());
            ipField.setDisable(true);
            btnIp.setText("Change");
        }

        changeIpActive = !changeIpActive;
    }
}
