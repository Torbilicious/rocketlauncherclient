package sample;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

import static sample.Messages.*;

/**
 * Created by TBinder on 14/12/15.
 */
public class KeyDownHandler implements EventHandler<KeyEvent>
{
    private final Messager messager;
    public static Messages lastMessage;

    public KeyDownHandler(Messager messager)
    {
        this.messager = messager;
    }

    public void handle(KeyEvent event)
    {
        Messages newMessage = null;
        
        switch (event.getCode())
        {
            case W:
                newMessage = UP;
                break;

            case A:
                newMessage = LEFT;
                break;

            case S:
                newMessage = DOWN;
                break;

            case D:
                newMessage = RIGHT;
                break;

            case SPACE:
                newMessage = FIRE;
                break;

//            case ENTER:
//                newMessage = STOP;
//                break;

            case Q:
                newMessage = LEDON;
                break;

            case E:
                newMessage = LEDOFF;
                break;
        }

        if (newMessage != lastMessage)
        {
            messager.sendMessage(newMessage);
            lastMessage = newMessage;
        }
    }
}
