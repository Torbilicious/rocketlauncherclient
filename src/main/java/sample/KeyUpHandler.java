package sample;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

/**
 * Created by TBinder on 14/12/15.
 */

public class KeyUpHandler implements EventHandler<KeyEvent>
{
    private final Messager messager;

    public KeyUpHandler(Messager messager)
    {
        this.messager = messager;
    }

    public void handle(KeyEvent event)
    {
        messager.sendMessage(Messages.STOP);
        KeyDownHandler.lastMessage = null;
    }
}
