package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class Main extends Application {
    Messager messager = new Messager();

    @Override
    public void start(Stage primaryStage) throws Exception{
        URL url = ClassLoader.getSystemResource("sample.fxml");

        Parent root = FXMLLoader.load(url);

        primaryStage.setTitle("\uD83D\uDE80 Amazing Rocket Launcher Client! \uD83D\uDE80");

        Scene scene = new Scene(root, 750, 50);
        scene.setOnKeyReleased(new KeyUpHandler(messager));
        scene.setOnKeyPressed(new KeyDownHandler(messager));

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
