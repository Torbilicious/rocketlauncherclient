package sample;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

/**
 * Created by TBinder on 24/11/15.
 */

public class Messager
{
    private static String ip = "127.0.0.1";

    public static void setIp(String ip)
    {
        Messager.ip = ip;
    }

    public synchronized void sendMessage(Messages message)
    {
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(String.format("http://%s:8080/direct?command=%s", ip, message.toString()));

        HttpResponse response = null;

        try
        {
            response = httpclient.execute(request);
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        System.out.println(response);
    }
}
