package sample;

/**
 * Created by TBinder on 14/12/15.
 */
public enum Messages
{
    UP,
    LEFT,
    DOWN,
    RIGHT,
    FIRE,
    STOP,
    LEDON,
    LEDOFF;
}
