package sample;

/**
 * Created by TBinder on 24/11/15.
 */

public class RocketLauncherConfig
{
    Integer x = 0;
    Integer y = 0;

    public RocketLauncherConfig(Integer x, Integer y)
    {
        this.x = x;
        this.y = y;
    }

    public Integer getX()
    {
        return x;
    }

    public Integer getY()
    {
        return y;
    }

    public void setX(Integer x)
    {
        this.x = x;
    }

    public void setY(Integer y)
    {
        this.y = y;
    }
}
